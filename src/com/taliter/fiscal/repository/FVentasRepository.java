package com.taliter.fiscal.repository;

import com.taliter.fiscal.models.FVentas;
import com.taliter.fiscal.util.PersistenceUtil;

public class FVentasRepository extends PersistenceUtil{

    public FVentas byFacture(String nFactura){
        FVentas fventas = (FVentas) this.whereOne("fventas", "numfactura='"+nFactura+"'");
        return fventas;
    }
}
