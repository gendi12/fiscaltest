package com.taliter.fiscal.util;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * User: waular
 * Date: 13/03/17
 */
public class PersistenceUtil {

    private static PersistenceUtil util = new PersistenceUtil();
    private static final String PACKAGE = "com.taluter.fiscal.models";

    public EntityManager getEntityManager() {
        EntityManager emanager = ConnectionUtils.getEntityManager();
        return emanager;
    }


    public EntityManager getEntityManager(String unitName) {
        EntityManager emanager = ConnectionUtils.getEntityManager(unitName);
        return emanager;
    }

    /****************************Hibernate 4 *************************************/


    public List<Object> list(String model) {
        Session session = null;
        try {
            session = HibernateConnector.getInstance().getSession();
            Query query = session.createQuery("from "+model+" m");

            List queryList = query.list();
            if (queryList != null && queryList.isEmpty()) {
                return null;
            } else {
                System.out.println("list " + queryList);
                return (List<Object>) queryList;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public Object find(String model, Long id) {
        Session session = null;
        try {
            session = HibernateConnector.getInstance().getSession();
            Query query = session.createQuery("from "+model+" m where m.id = :id");
            query.setParameter("id", id);

            List queryList = query.list();
            if (queryList != null && queryList.isEmpty()) {
                return null;
            } else {
                return queryList.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public Object whereOne(String model, String where) {
        Session session = null;
        try {
            session = HibernateConnector.getInstance().getSession();
            Query query = session.createQuery("from "+model+" m where 1=1 and "+where);

            List queryList = query.list();
            if (queryList != null && queryList.isEmpty()) {
                return null;
            } else {
                return queryList.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public List whereList(String model, String where) {
        Session session = null;
        List list=null;
        try {
            session = HibernateConnector.getInstance().getSession();
            Query query = session.createQuery("from "+model+" m where 1=1 and "+where);
            list = query.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return list;
    }

    public void update(Object model) {
        Session session = null;
        try {
            session = HibernateConnector.getInstance().getSession();
            session.saveOrUpdate(model);
            session.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Object add(Object object) {
        Session session = null;
        Transaction transaction = null;
        Object objectResponse=null;
        try {
            session = HibernateConnector.getInstance().getSession();
            System.out.println("session : "+session);
            transaction = session.beginTransaction();
            session.save(object);
            transaction.commit();
            objectResponse= object;
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return objectResponse;
    }

    public void delete(String model,int id) {
        Session session = null;
        try {
            session = HibernateConnector.getInstance().getSession();
            Transaction beginTransaction = session.beginTransaction();
            Query createQuery = session.createQuery("delete from "+model+" m where m.id =:id");
            createQuery.setParameter("id", id);
            createQuery.executeUpdate();
            beginTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }


    /***************************Fin Hibernate 4 *****************************************/







    public Object get(String model, Long id){
        Object modelObject=null;
        Class modelClass = null;
        try {
            modelClass = Class.forName(PACKAGE+"."+model);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        EntityManager emanager = util.getEntityManager();
        try{
            modelObject = emanager.find(modelClass, id);
            emanager.close();
        }catch (NoResultException e ){
            System.out.println("Sin resultado");
        }catch (ConstraintViolationException ex){
            System.out.println("Constrain violado");
        }
        return modelObject;
    }

    public List getList(String model){
        List list = null;
        try  {
            EntityManager emanager = util.getEntityManager();
            list = emanager.createQuery("FROM " + model + " m").getResultList();
            emanager.close();
        }catch(NoResultException e){
            e.printStackTrace();
            System.out.println("Error obteniendo la lista: " + e.getMessage());
        }
        return list;
    }

    public List getList(String model,String[] filters, String order){
        List list = null;
        String where = "where 1=1 ";
        if(filters != null){
            for(String filter : filters ) {
                where += " "+filter;
            }
        }

        if(order!=null&& !order.equals("")){
          order = "order by "+order;
        } else{
            order="";
        }
        try  {
            String query =  "FROM " + model + " m "+where+" "+order;
            EntityManager emanager = util.getEntityManager();
            list = emanager.createQuery(query).getResultList();
            emanager.close();
        }catch(NoResultException e){
            e.printStackTrace();
            System.out.println("Error obteniendo la lista: " + e.getMessage());
        }
        return list;
    }

    public static List maxRow(List list, int cant){
        List listMax= new ArrayList();
        for (int i=0; list.size()<=cant;i++){
            listMax.add(list.get(i));
        }
        return listMax;
    }

    public static Integer getLastId(String model){
        String sql = "SELECT MAX(id) FROM "+model;
        PersistenceUtil util= new PersistenceUtil();
        Integer objectId= (Integer)util.getEntityManager().createNativeQuery(sql).getSingleResult();
        return objectId;
    }


    public boolean insert(Object object){
        boolean execute = false;
        try {
            getEntityManager().persist(object);
            getEntityManager().getTransaction().commit();
            getEntityManager().close();
            execute= true;

        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager().close();
        }
        return execute;
    }


    public boolean insert(String unitName,Object object){
        boolean execute = false;
        try {
            getEntityManager(unitName).persist(object);
            getEntityManager(unitName).getTransaction().commit();
            getEntityManager(unitName).close();

            execute= true;
        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager(unitName).close();
        }
        return execute;
    }
/*
    public boolean update(Object object){
        boolean execute = false;
        try {
            getEntityManager().merge(object);
            getEntityManager().getTransaction().commit();
            getEntityManager().close();

            execute= true;

        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager().close();

        }
        return execute;
    }*/

    public boolean update(String unitName,Object object){
        boolean execute = false;
        try {
            getEntityManager(unitName).merge(object);
            getEntityManager(unitName).getTransaction().commit();
            getEntityManager(unitName).close();
            execute= true;
        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager(unitName).close();
        }
        return execute;
    }

    private boolean delete(Object object){
        boolean execute = false;
        try {
            getEntityManager().remove(object);
            getEntityManager().getTransaction().commit();
            getEntityManager().close();
            execute= true;
        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager().close();
        }
        return execute;
    }

    private boolean delete(String unitName,Object object){
        boolean execute = false;
        try {
            getEntityManager(unitName).remove(object);
            getEntityManager(unitName).getTransaction().commit();
            getEntityManager(unitName).close();

            execute= true;
        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager(unitName).close();

        }
        return execute;
    }

    public static Boolean exist(String model,String condition){
        return (Long) util.getEntityManager().createNativeQuery("select count(*) from "+model+" where "+condition).getSingleResult() > 0;
    }

    public String query(String table){
        String query="select * from "+table;
        return query;
    }

    public Long consultSequence(String sequence){
        Long result=null;

        Session session = null;
        String query="select "+sequence+".nextval as sequence from dual";
        try {
            session = HibernateConnector.getInstance().getSession();
            Transaction beginTransaction = session.beginTransaction();
            result = (Long) ((BigDecimal)session.createSQLQuery(query).uniqueResult()).longValue();
            //createQuery.executeUpdate();
            beginTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return result;
    }
}
