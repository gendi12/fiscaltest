package com.taliter.fiscal.util;


import com.google.gson.Gson;
import org.jsoup.Jsoup;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {


    public static String jsonByObject(Object object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }



    public static String  getCode (){
    	StringBuilder code = new StringBuilder();
        long milis = new java.util.GregorianCalendar().getTimeInMillis();
        Random random = new Random(milis);
        int iterable = 0;
        while ( iterable < 8){
            char chars = (char)random.nextInt(255);
            if ( (chars >= '0' && chars <='9') || (chars >='A' && chars <='Z') ){
                code.append(chars);
                iterable ++;
            }
        }
        return code.toString();
    }


    public static String getDateString(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(date);
    }

    public static Date getDateForString(String dateFormated){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = null;
        try {
            date = formatter.parse(dateFormated);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getDateForString(String dateFormated,String format){
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = formatter.parse(dateFormated);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    
    public static String getDateByAgile(String date){
         return getDateString(getDateForString(date));
    }
    public static String htmlToText(String html) {
        return Jsoup.parse(html).text();
    }

    public static String getMd5(String origin){

        MessageDigest mdisgest = null;
        try {
        	mdisgest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        mdisgest.update(origin.getBytes());
        byte[] digest = mdisgest.digest();
        StringBuffer sbuffer = new StringBuffer();
        for (byte b : digest) {
        	sbuffer.append(String.format("%02x", b & 0xff));
        }
        return  sbuffer.toString();
    }

    public static String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file), "UTF8")
        );
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         lseparator = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(lseparator);
            }

            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }
    
    public static String getLimitDates(Integer limit) {
    	String format = "";
    	Date date = new Date();
    	
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);

    	cal.add(Calendar.HOUR_OF_DAY, limit); 
    	
    	Calendar cal2 = Calendar.getInstance();
    	cal2.setTime(date);

    	Date dateBefore1Day = cal.getTime();
    	Date dateActual = cal2.getTime();
    	
    	DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
    	format = dateFormat.format(dateBefore1Day) + "-" + dateFormat.format(dateActual);
    	
    	return format;
    }

    
    public static String getFormatTimestampToString(String date) {
    	
    	String formatDate = "";
    	SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yy hh:mm:ss,SSSSSSSSS a");
    	
    	try {
    		formatDate = myFormat.format(fromUser.parse(date.replace("Z", "").replace("T", " ")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
    	return formatDate;
    }
    
    public static String getWordsByString(String line, int numberWords) {
    	
    	String words = "";
    	
    	Pattern pattern = Pattern.compile("([\\S]+\\s*){1,"+numberWords+"}");
        Matcher matcher = pattern.matcher(line);
        matcher.find();
        words = matcher.group().trim();
    	
    	return words;
    }
    
    public static boolean checkArticles(Integer idToCheck, String type, Object idsFromCheck) {
    	
    	boolean check = true;
    	
    	if(type.equals("home_secondary")) {
    		
    		System.out.println("Check The Home Secundaries Id ("+idToCheck+") From Channels Mains Ids...");
    		
    		if(idsFromCheck instanceof List<?>) {
    			List<Map<String, Integer>> channelsMainIds = (List<Map<String, Integer>>) idsFromCheck;
    			
    			for(Object channelMainId : channelsMainIds) {
    				Map<String, Integer> channelMainIdCast = (Map<String, Integer>) channelMainId;
    				
    				for (Map.Entry<String, Integer> entry : channelMainIdCast.entrySet()) {
    					if(String.valueOf(entry.getValue()).equals(String.valueOf(idToCheck))) {
    						System.out.println("\n");
    						System.out.println("Repeat Id (home_secondary): ("+entry.getKey()+")" + idToCheck);
    						System.out.println("\n");
    						check = false;
    					}
    				}
    			}
    		}
    	} else if(type.equals("channel_secondary")) {
    		if(idsFromCheck instanceof List<?>) {
    			List<Integer> homeSecundayIds = (List<Integer>) idsFromCheck;
    			
    			for(Integer homeSecundayId : homeSecundayIds) {
    				if(String.valueOf(homeSecundayId).equals(String.valueOf(idToCheck))) {
    					System.out.println("\n");
    					System.out.println("Repeat Id (channel_secondary): " + idToCheck);
    					System.out.println("\n");
						check = false;
					}
    			}
    		}
    	}
    	
    	return check;
    }

    /**
     * Funci?n que elimina acentos y caracteres especiales de
     * una cadena de texto.
     * @param input
     * @return cadena de texto limpia de acentos y caracteres especiales.
     */
    public static String stringToURL(String input) {
        // Cadena de caracteres original a sustituir.

        String output = Normalizer
                .normalize(input, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");

        return output.replace(" ","-").toLowerCase();
    }

    public static void writeFile(String fileName, String path, String newText) {
		
		String txtPath = path + fileName;
        File file = new File(txtPath);
        BufferedWriter bw = null;
        
        if(file.exists()) {
            try {
				bw = new BufferedWriter(new FileWriter(file, true));
				bw.write(newText);
				bw.write("\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
        } else {
            try {
				bw = new BufferedWriter(new FileWriter(file));

				bw.write(newText + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
        
        try {
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    public static void saveFile(String fileName, String path, String newText) {

        String txtPath = path + fileName;
        File file = new File(txtPath);
        BufferedWriter bw = null;
    	

            try {
                bw = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(file), "UTF8"));
                bw.write(newText);
              
            } catch (IOException e) {
                e.printStackTrace();
            }

        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static StringBuffer saveFileHtml(String fileName, String path, String newText) {

        String txtPath = path + fileName;
        File file = new File(txtPath);
        BufferedWriter bw = null;
    	StringBuffer valueLog = new StringBuffer();

            try {
                bw = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(file), "UTF8"));
                bw.write(newText);
                valueLog.append(" Se Genero el Archivo Html Satisfactoriamente");
            } catch (IOException e) {
                e.printStackTrace();
                valueLog.append("Se Genero la siguiente Excepcion:");
 	            valueLog.append(e);
            }

        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return valueLog;
    }


    public static String convertByteToHex(byte data[]) {
        StringBuffer hexData = new StringBuffer();

        for (int byteIndex = 0; byteIndex < data.length; byteIndex++) {
            hexData.append(Integer.toString((data[byteIndex] & 0xff) + 0x100, 16).substring(1));
        }
        return hexData.toString();
    }

    /*  metodo que recibe la imagen en string Base64 y la decodifica y guarda en disco     */

    public static  boolean saveImageBase64(String fileName, String path,String imageBase64) {
        boolean save=false;
        String[] parts = imageBase64.split(",");
        String imageString = parts[1];
        String fileExtension = parts[0].split(";")[0].split(":")[1].split("/")[1];
            byte[] imageByte= com.sun.org.apache.xerces.internal.impl.dv.util.Base64.decode(imageString);
        try {
            File file = null;
            if(Util.createFolder(path)){
                file= new File(path+fileName+"."+fileExtension);
                try (BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(file))) {
                    writer.write(imageByte);
                    writer.flush();
                    writer.close();
                }
                save = true;
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return save;
    }
    public static  String getExtension(String imageBase64){
        String[] parts = imageBase64.split(",");
        String fileExtension = parts[0].split(";")[0].split(":")[1].split("/")[1];
        return fileExtension;
    }

    public static boolean createFolder(String path){
        boolean exits = false;
        File theDir = new File(path);
        if (!theDir.exists()) {
            try{
                exits = theDir.mkdir();
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }else{
            exits=true;
        }
        return exits;
    }


    /*  metodo que recibe un parametro y busca una imagen y la codifica y la retorna en un String Base64      */
    public static String encode(String nombre) {
        String encodedString ="";
        List<File> list = new ArrayList<File>();
        list.add(new File("/mnt/data01/eluniversal/euroot/agile/"+nombre+".jpg"));
        list.add(new File("/mnt/data01/eluniversal/euroot/agile/"+nombre+".gif"));
        list.add(new File("/mnt/data01/eluniversal/euroot/agile/"+nombre+".png"));
        list.add(new File("/mnt/data01/eluniversal/euroot/agile/"+nombre+".jpeg"));

        try{


            for(File def  : list){

                if(def.exists()){

                    byte[] buffer;
                    try (BufferedInputStream bufferis = new BufferedInputStream(new
                            FileInputStream(def))) {
                        int bytes = (int) def.length();
                        buffer = new byte[bytes];
                        int readBytes = bufferis.read(buffer);
                        bufferis.close();
                    }
                    encodedString = com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(buffer);
                }
            }


            /*Codificamos a base 64*/

            return  encodedString;

        }catch(IOException e){

            e.getMessage();
            e.getCause();
            return null;

        }
    }

}
