package  com.taliter.fiscal.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Clase para el manejo de conexiones a base de datos
 * @author waular
 */

public class ConnectionUtils {
    private static EntityManagerFactory entityManagerFactory = null;
    private static EntityManager entityManager = null;
    private static Connection connection = null;


    /**
     * Crea un objeto de tipo Entity Manager para la persistencia de Objetos en Base de Datos
     * @return EntityManager (Objeto para el manejo de la persistencia)
     */
    public static EntityManager getEntityManager() {
        if (entityManagerFactory == null || !entityManager.isOpen() || !entityManagerFactory.isOpen()){
            entityManagerFactory = Persistence.createEntityManagerFactory("postgres");
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
        }
        return entityManager;
    }
    
    public static EntityManager getEntityManager(String unitName) {
        if (entityManagerFactory == null || !entityManager.isOpen() || !entityManagerFactory.isOpen()){
            entityManagerFactory = Persistence.createEntityManagerFactory(unitName);
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
        }
        return entityManager;
    }
    
    public static void closeConnection() {
        try {
            if (entityManagerFactory!=null){
                entityManagerFactory.close();}
            if (connection!=null){
                connection.close();}
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



}
