package com.taliter.fiscal.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "fdetalles")
public class FDetalles {

    @Column
    private String hora;
    @Column
    private Integer sucursal;
    @Column
    private Timestamp utime;
    @Column
    private Date fecha;
    @Id
    @Column
    private String numfactura;
    @Column
    private String tipofactura;
    @Column
    private Integer fcant;
    @Column
    private String fiten;
    @Column
    private Double fpreuni;
    @Column
    private Double fprecio;
    @Column
    private Integer orden;
    @Column(name = "status_migracion")
    private Integer statusMigration;

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Integer getSucursal() {
        return sucursal;
    }

    public void setSucursal(Integer sucursal) {
        this.sucursal = sucursal;
    }

    public Timestamp getUtime() {
        return utime;
    }

    public void setUtime(Timestamp utime) {
        this.utime = utime;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNumfactura() {
        return numfactura;
    }

    public void setNumfactura(String numfactura) {
        this.numfactura = numfactura;
    }

    public String getTipofactura() {
        return tipofactura;
    }

    public void setTipofactura(String tipofactura) {
        this.tipofactura = tipofactura;
    }

    public Integer getFcant() {
        return fcant;
    }

    public void setFcant(Integer fcant) {
        this.fcant = fcant;
    }

    public String getFiten() {
        return fiten;
    }

    public void setFiten(String fiten) {
        this.fiten = fiten;
    }

    public Double getFpreuni() {
        return fpreuni;
    }

    public void setFpreuni(Double fpreuni) {
        this.fpreuni = fpreuni;
    }

    public Double getFprecio() {
        return fprecio;
    }

    public void setFprecio(Double fprecio) {
        this.fprecio = fprecio;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getStatusMigration() {
        return statusMigration;
    }

    public void setStatusMigration(Integer statusMigration) {
        this.statusMigration = statusMigration;
    }


}
