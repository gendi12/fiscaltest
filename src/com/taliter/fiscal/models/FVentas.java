package com.taliter.fiscal.models;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "fventas")
public class FVentas {

    @Column
    private String hora;

    @Column
    private String dni;
    @Column
    private String clientenombre;
    @Column
    private Double monto;
    @Column
    private Double xefectivo;
    @Column
    private Double xbanco;
    @Column
    private Double xcheque;
    @Column
    private String numcheque;
    @Id
    @Column
    private String numfactura;
    @Column
    private String tipofactura;
    @Column
    private Integer sucursal;
    @Column
    private Timestamp ultime;
    @Column
    private String fecha;
    @Column
    private Boolean borrado;

    @Column
    private Integer statusMigration;

    @Transient
    private List<FDetalles> detalles;

    public Integer getStatusMigration() {
        return statusMigration;
    }

    public void setStatusMigration(Integer statusMigration) {
        this.statusMigration = statusMigration;
    }

    public List<FDetalles> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<FDetalles> detalles) {
        this.detalles = detalles;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getClientenombre() {
        return clientenombre;
    }

    public void setClientenombre(String clientenombre) {
        this.clientenombre = clientenombre;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Double getXefectivo() {
        return xefectivo;
    }

    public void setXefectivo(Double xefectivo) {
        this.xefectivo = xefectivo;
    }

    public Double getXbanco() {
        return xbanco;
    }

    public void setXbanco(Double xbanco) {
        this.xbanco = xbanco;
    }

    public Double getXcheque() {
        return xcheque;
    }

    public void setXcheque(Double xcheque) {
        this.xcheque = xcheque;
    }

    public String getNumcheque() {
        return numcheque;
    }

    public void setNumcheque(String numcheque) {
        this.numcheque = numcheque;
    }

    public String getNumfactura() {
        return numfactura;
    }

    public void setNumfactura(String numfactura) {
        this.numfactura = numfactura;
    }

    public String getTipofactura() {
        return tipofactura;
    }

    public void setTipofactura(String tipofactura) {
        this.tipofactura = tipofactura;
    }

    public Integer getSucursal() {
        return sucursal;
    }

    public void setSucursal(Integer sucursal) {
        this.sucursal = sucursal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Timestamp getUltime() {
        return ultime;
    }

    public void setUltime(Timestamp ultime) {
        this.ultime = ultime;
    }
}
