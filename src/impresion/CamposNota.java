package impresion;

import java.util.ArrayList;

public class CamposNota {

	private String nName, nDireccion, nNumeroFactura, nIvaType, nDocumento, nFacturaType, monto, sucursal;
	private ArrayList<String[]> nItems;
	
	public CamposNota(String nName, String nDireccion, String nDocumento, String nIvaType, String nNumeroFactura,
			String nFacturaType, String nMonto, String sucursal) {
		super();
		this.nName = nName;
		this.nDireccion = nDireccion;
		this.nDocumento = nDocumento;
		this.nIvaType = nIvaType;
		this.nNumeroFactura = nNumeroFactura;
		this.nFacturaType = "NC" + nFacturaType;
		this.monto = nMonto;
		this.sucursal = sucursal;
	}
	
	public CamposNota() {
		super();
	}

	public String getnName() {
		return nName;
	}

	public void setnName(String nName) {
		this.nName = nName;
	}

	public String getnDocumento() {
		return nDocumento;
	}

	public void setnDocumento(String nDocumento) {
		this.nDocumento = nDocumento;
	}

	public String getnDireccion() {
		return nDireccion;
	}

	public void setnDireccion(String nDireccion) {
		this.nDireccion = nDireccion;
	}

	public String getnNumeroFactura() {
		return nNumeroFactura;
	}

	public void setnNumeroFactura(String nNumeroFactura) {
		this.nNumeroFactura = nNumeroFactura;
	}

	public ArrayList<String[]> getnItems() {
		return nItems;
	}

	public void setnItems(ArrayList<String[]> nItems) {
		this.nItems = nItems;
	}

	public String getnIvaType() {
		return nIvaType;
	}

	public void setnIvaType(String nIvaType) {
		this.nIvaType = nIvaType;
	}

	public String getnFacturaType() {
		return nFacturaType;
	}

	public void setnFacturaType(String nFacturaType) {
		this.nFacturaType = "NC" + nFacturaType;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	
	

}
