package impresion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DataController {
	
    private static final DataController ourInstance = new DataController();

    static DataController getInstance() {
        return ourInstance;
    }
    
    private DataController(){
    	PostgreSQLJDBC.getInstance();
    }
    
	   // Seleccionar la factura de tabla fventas, y sacar el Documento de Identificacion "dni"    
    public static String[] getFVentas(String numeroFactura, String tipoFactura){
    	final String selectFVenta = "SELECT dni,sucursal FROM fventas WHERE numfactura = '"+ numeroFactura +"' AND tipofactura = '" + tipoFactura + "'";
    	
    	try {
    		ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase(selectFVenta);
    		mResultSet.next();
			return new String[]{mResultSet.getString(1),mResultSet.getString(2)};
		} catch (SQLException e) {

			e.printStackTrace();
		}
    	return null;
    }
    
	   // Seleccionar el cliente en la tabla clientes con el dato del DNI, sacar los datos de "nombre/direccion/dni/resiva"
    public static String[] getClientes(String dni){
    	final String selectClientes = "SELECT nombre,direccion,dni,resiva FROM clientes WHERE dni = '" + dni + "'";
    	
    	try {
    		ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase(selectClientes);
			mResultSet.next();
			return new String[]{mResultSet.getString(1),mResultSet.getString(2),mResultSet.getString(3),mResultSet.getString(4)};
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
	   // Seleccionar los ITEMS de la tabla FDetalles con el dato del numfactura/tipofactura, sacar los datos de "fitem/fcant/fpreuni"
    public static ArrayList<String[]> getFDetalles(String numeraFactura, String tipoFactura){
    	final String selectFDetalle = "SELECT fitem,fcant,fpreuni FROM fdetalles WHERE numfactura = '" + numeraFactura + "' AND tipofactura = '" + tipoFactura + "'";
    	
    	ArrayList<String[]> mData = new ArrayList<String[]>();
    	
    	try {
    		ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase(selectFDetalle);
			while(mResultSet.next()){
			    mData.add(new String[]{mResultSet.getString(1),mResultSet.getString(2),mResultSet.getString(3)}); 
			}
			return mData;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String[] getNCredito(){
    	final String selectFDetalle = "SELECT dni,casoesp FROM fistmpcab WHERE clientenombre = 'NCredito' LIMIT 1;";
    	
    	try {
    		ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase(selectFDetalle);
			mResultSet.next();    
			return new String[]{mResultSet.getString(1),mResultSet.getString(2)};
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
    	return null;
    }
    
    public static void deleteNCredito(String numeraFactura){
    	final String deleteNCredito = "DELETE FROM fistmpcab WHERE dni = '" + numeraFactura + "';";
    	try {
    		PostgreSQLJDBC.executeQueryDataBase(deleteNCredito);
		} catch (Exception e) {
		
			e.printStackTrace();
		}
    }
    
    public static void setDBName(String data){
    	PostgreSQLJDBC.setDBName(data);
    }
    
    public static void setServer(String data){
    	PostgreSQLJDBC.setServer(data);
    }
    
    public static String getCOMPORT(){
    	final String selectFVenta = "select valor2 from parametros where pname = 'fiscalimp'";
    	
    	
    	try {
    		ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase(selectFVenta);
    		mResultSet.next();
			return "COM" + mResultSet.getString(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String createRow(){
    	final String selectFVenta = "ALTER TABLE fventas ADD COLUMN ref_num character varying(100);";
    	
    	try {
    		PostgreSQLJDBC.executeQueryDataBase(selectFVenta);
    		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
    
    public static void register(String facturaNUM, CamposNota perfil){
    	String [] prefijo = perfil.getnNumeroFactura().split("-");
    	facturaNUM = prefijo[0] + "-" + facturaNUM;
    	final String selectFVenta = "INSERT INTO fventas(hora, dni, clientenombre, monto, numfactura, tipofactura, sucursal, utime, fecha, borrado, ref_num)"
    			+ "VALUES (current_time,'" + perfil.getnDocumento() + "','"+ perfil.getnName() +"'," + perfil.getMonto() + ",'" + facturaNUM + "','" + perfil.getnFacturaType() + "'," + perfil.getSucursal() + ", current_timestamp, current_date,FALSE,'" + perfil.getnNumeroFactura() + "'); ";
    	try {
    		PostgreSQLJDBC.executeQueryDataBase(selectFVenta);
    		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static String searchFacturaHeader(){
    	final String selectFVenta = "select valor3 from parametros where pname = 'fiscalimp'";
    	
    	
    	try {
    		ResultSet mResultSet = PostgreSQLJDBC.executeQueryDataBase(selectFVenta);
    		mResultSet.next();
			return mResultSet.getString(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
}
