package impresion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.TimerTask;

import impresion.ImprimirFiscal;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class Ventana extends JFrame implements PanelFactura.OnCompleteListener{

	/**
	 * 
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PanelClientes panelClientes;
	private PanelFactura panelFactura;
	private PanelItems panelItems;
	private static String nFactura;
	private static String tFactura;
	private JTextField textSerialPort;
	private JTextField textFieldServer;
	private CamposNota perfil;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		
		DataController dc = DataController.getInstance();
		
		setResizable(false);
		getContentPane().setBackground(new Color(250, 240, 230));
		setBackground(SystemColor.scrollbar);
		setBounds(100, 100, 411, 331);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		panelClientes = new PanelClientes();
		panelClientes.setBounds(10, 11, 253, 104);
		getContentPane().add(panelClientes);
		
		panelItems = new PanelItems();
		panelItems.setBounds(10, 149, 389, 150);
		getContentPane().add(panelItems);
	
		panelFactura = new PanelFactura(this);
		panelFactura.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Factura N\u00B0", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 255)));
		panelFactura.setBounds(262, 11, 137, 76);
		getContentPane().add(panelFactura);
		
		JButton btnNewButton_1 = new JButton("Nota de Credito");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
		
				try {
					ImprimirFiscal.setComPort(DataController.getCOMPORT().toUpperCase());
					ImprimirFiscal.cargarCLiente(perfil.getnName(), perfil.getnDocumento(), perfil.getnDireccion(), perfil.getnFacturaType(), perfil.getnIvaType());
					ImprimirFiscal.cargarRemito(perfil.getnNumeroFactura());
					
					ImprimirFiscal.abrirRemito(perfil.getnFacturaType());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for (String[] object: panelItems.getTableItems()) {
					try {
						ImprimirFiscal.cargarItem(object[0],object[1],object[2],panelFactura.getFacturaType());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				Double precio = ImprimirFiscal.getmontoTotal() * -1;
				DecimalFormat df = new DecimalFormat("0.0000");
				perfil.setMonto(df.format(precio).replace(",", "."));
				
				try {	
				
					DataController.register(ImprimirFiscal.cerrarRemito(),perfil);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
	
		btnNewButton_1.setBounds(10, 115, 136, 23);
		getContentPane().add(btnNewButton_1);
		
		textSerialPort = new JTextField();
		textSerialPort.setEnabled(false);
		textSerialPort.setEditable(false);
		textSerialPort.setBounds(212, 126, 50, 20);
		getContentPane().add(textSerialPort);
		textSerialPort.setColumns(10);
		
		textFieldServer = new JTextField();
		textFieldServer.setEnabled(false);
		textFieldServer.setEditable(false);
		textFieldServer.setBounds(263, 126, 136, 20);
		getContentPane().add(textFieldServer);
		textFieldServer.setColumns(10);
		
		readConfFile();
		
		DataController.setDBName(textSerialPort.getText());
		DataController.setServer(textFieldServer.getText());
		
		
		//Timer timer = new Timer();
		//timer.scheduleAtFixedRate(tarea1(), 2*2000, 2*2000);
		DataController.createRow();
		process();
	}
	
	public void process(){
		
		System.out.println("Process->");
		try {
		  /*  String line;
		    Process p = Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
		    BufferedReader input =
		            new BufferedReader(new InputStreamReader(p.getInputStream()));
		    while ((line = input.readLine()) != null) {
		        System.out.println(line); //<-- Parse data here.
		    }
		    input.close();*/
			
			 Runtime.getRuntime().exec("taskkill /F /IM " + "dbsynker.exe");
		} catch (Exception err) {
		    err.printStackTrace();
		}
	}
	
	public TimerTask tarea1() {
		return new TimerTask() {
			  @Override
			  public void run() {

				try {
					DataController dc = DataController.getInstance();
					String[] hola = DataController.getNCredito();
					if (hola != null){
						  nFactura = hola[0];
						  tFactura = hola[1];
						  
						  ImprimirFiscal.setComPort(DataController.getCOMPORT().toUpperCase());
						  DataController.deleteNCredito(nFactura);
						  //String dni = dc.getFVentas(nFactura, tFactura);
							
						  //System.out.println(dni);
						
						  panelItems.setTableItems(dc.getFDetalles(nFactura, tFactura));
						  //panelClientes.setCampos(dc.getClientes(dni));
					  
					}
				  } catch (Exception e) {
					  
				  }
						  
			  }
		};
	}
	
	public void readConfFile(){
		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader("config.txt"));
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    
		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    String everything = sb.toString();
		    String hola[] = everything.split(";");
		    if (hola.length != 0){
		    	textFieldServer.setText(hola[0]);
		    	textSerialPort.setText(hola[1]);
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
		    		 e.getMessage(),
		    		    "DB ERROR",
		    		    JOptionPane.ERROR_MESSAGE);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	
	@Override
	public void OnComplete(String text) {
		// TODO Auto-generated method stub
		panelClientes.setCampos(new String[]{"","","",""});
		panelItems.setTableItems(null);
		String factura;
		if (text.length() <= 8) factura = DataController.searchFacturaHeader() + "-" + "00000000".substring(text.length()) + text;
		else factura = text;
		
		panelFactura.setFracturaString(factura);
		
		System.out.println("factura " + factura);
		String type = panelFactura.getFacturaType();
		String[] data = DataController.getFVentas(factura, type);
		
		if (data == null) return;
	
		panelItems.setTableItems(DataController.getFDetalles(factura, type));
		String [] resultado;
		if(data[0].isEmpty()) resultado = new String[]{"Cosumidor Final", " ", " ","2"};
		else resultado = DataController.getClientes(data[0]);
		panelClientes.setCampos(resultado);
		
		perfil = new CamposNota(resultado[0],resultado[1],resultado[2],resultado[3],factura,type,null,data[1]);
		perfil = new CamposNota(resultado[0],resultado[1],resultado[2],resultado[3],factura,type,null,data[1]);
	}
}
