package impresion;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PanelClientes extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textClientName;
	private JTextField textCuitNumber;
	private JTextField textDireccion;
	private String clienType; 

	/**
	 * Create the panel.
	 */
	public PanelClientes() {
		setBorder(new TitledBorder(null, "Datos del Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		setLayout(new GridLayout(3, 2, 5, 5));
		
		JLabel lblNewLabel = new JLabel("Nombre del Cliente");
		add(lblNewLabel);
		
		textClientName = new JTextField();
		add(textClientName);
		textClientName.setColumns(10);
		
		JLabel lblCuit = new JLabel("C.U.I.T");
		add(lblCuit);
		
		textCuitNumber = new JTextField();
		add(textCuitNumber);
		textCuitNumber.setColumns(10);
		
		JLabel lblDireccion = new JLabel("Direccion");
		add(lblDireccion);
		
		textDireccion = new JTextField();
		add(textDireccion);
		textDireccion.setColumns(10);

	}
	
	public String textClientNameString(){
		String mText = textClientName.getText();
		if (mText.isEmpty()) return " ";
		else return mText;
	}
	
	public String textCuitNumberString(){
		String mText = textCuitNumber.getText();
		if (mText.isEmpty()) return " ";
		else return mText;
	}
	
	public String textDireccionString(){
		String mText = textDireccion.getText();
		if (mText.isEmpty()) return " ";
		else return mText;
	}
	

	public void setCampos(String [] arg0){
		textClientName.setText(arg0[0]);
		textDireccion.setText(arg0[1]);
		textCuitNumber.setText(arg0[2]);
		clienType = arg0[3];
		
	}

	public String getClienType() {
		return clienType;
	}

	public void setClienType(String clienType) {
		this.clienType = clienType;
	}

	
	
}
