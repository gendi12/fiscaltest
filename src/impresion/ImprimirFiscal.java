package impresion;

import java.text.DecimalFormat;

import com.taliter.fiscal.device.FiscalDevice;
import com.taliter.fiscal.device.FiscalDeviceSource;
import com.taliter.fiscal.device.FiscalPacket;
import com.taliter.fiscal.device.hasar.HasarConstants;
import com.taliter.fiscal.device.hasar.HasarFiscalDeviceSource;
import com.taliter.fiscal.port.rxtx.RXTXFiscalPortSource;
import com.taliter.fiscal.util.LoggerFiscalDeviceEventHandler;
import com.taliter.fiscal.util.LoggerFiscalPortSource;

public class ImprimirFiscal implements HasarConstants{
	
	private static String comPort = "COM1";
	private static double montoTotal=0;
	
	public static void abrirRemito(String type) throws Exception{
		montoTotal = 0;
		String tipoFactura = null;
		if (type.contains("A")) tipoFactura = "R";
		else if (type.contains("B")) tipoFactura = "S";
		FiscalDeviceSource deviceSource = new HasarFiscalDeviceSource(new RXTXFiscalPortSource(comPort));

		boolean logComm = false;
		if (logComm) deviceSource.setPortSource(new LoggerFiscalPortSource(deviceSource.getPortSource(), System.out));
		
		FiscalDevice device = deviceSource.getFiscalDevice();

		device.setEventHandler(new LoggerFiscalDeviceEventHandler(System.out));

		device.open();	// Open the device and synchronize communications.

		try		// Close the device even in the event of an error.
		{
			FiscalPacket request = device.createFiscalPacket();	// Create a request packet.
			request.setCommandCode(CMD_OPEN_NFHD);			// Set the appropriate command code.
			request.setSize(2);
			request.setString(1, tipoFactura);
			request.setString(2, "T");
			FiscalPacket response = device.execute(request);	// Execute the request.
			
			System.out.println(response.toString());	// Output the returned date.
		}
		finally { device.close(); }		// Close the device.
	}
	
	public static void cargarCLiente(String nombre,String numeroFiscal, String lugar, String type, String typeClient) throws Exception{
		String clientType = "I";
		String dniType = "C";
		
		if (typeClient.equals("2")){
			clientType = "C";
			dniType = " ";
		}else if(typeClient.equals("1")){
			clientType = "S";
			dniType = "C";
		}else if(typeClient.equals("0")){
			clientType = "I";
			dniType = "C";
		}
		
		FiscalDeviceSource deviceSource = new HasarFiscalDeviceSource(new RXTXFiscalPortSource(comPort));

		nombre = nombre.substring(0,Math.min(nombre.length(), 40));
		lugar = lugar.substring(0,Math.min(lugar.length(), 40));
		
		boolean logComm = false;
		if (logComm) deviceSource.setPortSource(new LoggerFiscalPortSource(deviceSource.getPortSource(), System.out));
		
		FiscalDevice device = deviceSource.getFiscalDevice();

		device.setEventHandler(new LoggerFiscalDeviceEventHandler(System.out));

		device.open();	// Open the device and synchronize communications.

		try		// Close the device even in the event of an error.
		{
			FiscalPacket request = device.createFiscalPacket();	// Create a request packet.
			request.setCommandCode(CMD_SET_CUSTOMER_DATA);			// Set the appropriate command code.
			request.setSize(5);
			request.setString(1, nombre);
			request.setString(2, numeroFiscal);
			request.setString(3, clientType);
			request.setString(4, dniType);
			request.setString(5, lugar);
			FiscalPacket response = device.execute(request);	// Execute the request.
			
			System.out.println(response.toString());	// Output the returned date.
		}
		finally { device.close(); }		// Close the device.
	}
	
	public static void cargarRemito(String numeroFactura) throws Exception{
			
			FiscalDeviceSource deviceSource = new HasarFiscalDeviceSource(new RXTXFiscalPortSource(comPort));
	
			boolean logComm = false;
			if (logComm) deviceSource.setPortSource(new LoggerFiscalPortSource(deviceSource.getPortSource(), System.out));
			
			FiscalDevice device = deviceSource.getFiscalDevice();
	
			device.setEventHandler(new LoggerFiscalDeviceEventHandler(System.out));
	
			device.open();	// Open the device and synchronize communications.
	
			try		// Close the device even in the event of an error.
			{
				FiscalPacket request = device.createFiscalPacket();	// Create a request packet.
				request.setCommandCode(CMD_SET_EMBARK_NUMBER);			// Set the appropriate command code.
				request.setSize(2);
				request.setString(1, "1");
				request.setString(2, numeroFactura);
				FiscalPacket response = device.execute(request);	// Execute the request.
				
				System.out.println(response.toString());	// Output the returned date.
			}
			finally { device.close(); }		// Close the device.
		}


	public static void cargarItem(String descripcion, String cantidad, String monto, String type) throws Exception{
		Float precio = Float.parseFloat(monto);
		montoTotal += precio;
		
		//if (type.equals("A")) precio = precio / (1+(21/precio));
		if (type.equals("A")) precio = (float) (precio * (82.6446 /100));
		else if (type.equals("B")) precio = ((precio*100)/121);
		DecimalFormat df = new DecimalFormat("0.0000");
		String sMonto = df.format(precio).replace(",", ".");
		
		
		
		df = new DecimalFormat("0.0");
		String sCant = df.format(Float.parseFloat(cantidad)).replace(",", ".");
		
		if (descripcion.isEmpty()) descripcion = " ";
		else descripcion = descripcion.substring(0,Math.min(descripcion.length(), 20));
		
		FiscalDeviceSource deviceSource = new HasarFiscalDeviceSource(new RXTXFiscalPortSource(comPort));
	
		boolean logComm = false;
		if (logComm) deviceSource.setPortSource(new LoggerFiscalPortSource(deviceSource.getPortSource(), System.out));
		
		FiscalDevice device = deviceSource.getFiscalDevice();
	
		device.setEventHandler(new LoggerFiscalDeviceEventHandler(System.out));
	
		device.open();	// Open the device and synchronize communications.
	
		try		// Close the device even in the event of an error.
		{
			FiscalPacket request = device.createFiscalPacket();	// Create a request packet.
			request.setCommandCode(CMD_PRINT_LINE_ITEM);			// Set the appropriate command code.
			request.setSize(8);
			request.setString(1, descripcion);
			request.setString(2, sCant);
			request.setString(3, sMonto);
			request.setString(4, "21.0");
			request.setString(5, "M");
			request.setString(6, "0.0");
			request.setString(7, "0");
			request.setString(8, "B");
			FiscalPacket response = device.execute(request);	// Execute the request.
			
			System.out.println(response.toString());	// Output the returned date.
		}
		finally { device.close(); }		// Close the device.
	}

	public static String cerrarRemito() throws Exception{
		String strResponse = "";
		FiscalDeviceSource deviceSource = new HasarFiscalDeviceSource(new RXTXFiscalPortSource(comPort));

		boolean logComm = false;
		if (logComm) deviceSource.setPortSource(new LoggerFiscalPortSource(deviceSource.getPortSource(), System.out));
		
		FiscalDevice device = deviceSource.getFiscalDevice();

		device.setEventHandler(new LoggerFiscalDeviceEventHandler(System.out));

		device.open();	// Open the device and synchronize communications.

		try		// Close the device even in the event of an error.
		{
			FiscalPacket request = device.createFiscalPacket();	// Create a request packet.
			request.setCommandCode(CMD_CLOSE_NFHD);			// Set the appropriate command code.
			FiscalPacket response = device.execute(request);	// Execute the request.
			strResponse = response.toString();
			String vectorResponse[] = strResponse.split(" ");
			strResponse = vectorResponse[4].replaceAll("\"", "");
			
			System.out.println(response.toString() + " " + strResponse);	// Output the returned date.
		}
		finally { 
			device.close(); 
		}		// Close the device.
		return strResponse;
	}
	
	public static void reporteZ() throws Exception{
		
		FiscalDeviceSource deviceSource = new HasarFiscalDeviceSource(new RXTXFiscalPortSource(comPort));

		boolean logComm = false;
		if (logComm) deviceSource.setPortSource(new LoggerFiscalPortSource(deviceSource.getPortSource(), System.out));
		
		FiscalDevice device = deviceSource.getFiscalDevice();

		device.setEventHandler(new LoggerFiscalDeviceEventHandler(System.out));

		device.open();	// Open the device and synchronize communications.

		try		// Close the device even in the event of an error.
		{
			FiscalPacket request = device.createFiscalPacket();	// Create a request packet.
			request.setCommandCode(CMD_DAILY_CLOSE);			// Set the appropriate command code.
			request.setSize(1);
			request.setString(1, "Z");
			FiscalPacket response = device.execute(request);	// Execute the request.
			
			System.out.println(response.toString());	// Output the returned date.
		}
		finally { device.close(); }		// Close the device.
	}

	public static void setComPort(String comPort) {
		if (!comPort.isEmpty()) ImprimirFiscal.comPort = comPort;
		
	}

	public static double getmontoTotal() {
		return montoTotal;
	}


}
