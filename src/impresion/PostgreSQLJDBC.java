package impresion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class PostgreSQLJDBC {
	
		private static Connection mConnection = null;
		private static final String SQL_POSTGRESQL_PROVIDER = "org.postgresql.Driver";
		private static final String SQL_CONNECTION_STRING = "jdbc:postgresql://";
		private static final String SQL_CONNECTION_PORT = ":5432/";
		private static final String SQL_CONNECTION_USER = "postgres";
		private static final String SQL_CONNECTION_PASS = "wkrdjqwnd";
		private static String serverName = "localhost";
		private static String mDBName = "lean";
		/*private String mServer;
		private String mPort;
		private String mTable;
		private String mPostgres;
		private String mPassword;*/
		
	    private static final PostgreSQLJDBC ourInstance = new PostgreSQLJDBC();

	    static PostgreSQLJDBC getInstance() {
	        return ourInstance;
	    }
	    
	    private PostgreSQLJDBC(){
	    	
	    }
	    
	    public static void setDBName(String stringDBName){
	    	if (!stringDBName.isEmpty()) mDBName = stringDBName;
	    }
	    
	    public static void setServer(String stringserverName){
	    	if (!stringserverName.isEmpty()) serverName = stringserverName;
	    }
	   
	   private static boolean abrirDataBase(){
			  try {
			     Class.forName(SQL_POSTGRESQL_PROVIDER);
			     mConnection = DriverManager.getConnection(SQL_CONNECTION_STRING + serverName + SQL_CONNECTION_PORT + mDBName, SQL_CONNECTION_USER, SQL_CONNECTION_PASS);
			     
			     System.out.println("Opened database successfully");
			     return true;  
			  } catch (Exception e) {
			     e.printStackTrace();
			     System.err.println(e.getClass().getName()+": "+e.getMessage());
			     JOptionPane.showMessageDialog(null,
			    		 e.getMessage(),
			    		    "DB ERROR",
			    		    JOptionPane.ERROR_MESSAGE);
			    // System.exit(0);
			     return false; 
			  }
	   }
	   
	   private static boolean cerrarDataBase(){
			  try {
			     mConnection.close();
			     
			     System.out.println("DataBase Close successfully");
			     return true;  
			  } catch (Exception e) {
			     e.printStackTrace();
			     System.err.println(e.getClass().getName()+": "+e.getMessage());
			    // System.exit(0);
			     return false; 
			  }
	   }
	 
	  
	   public static ResultSet executeQueryDataBase(String mQuery){
			  try {
				 abrirDataBase();
				 
			     Statement mConnectionStatement = mConnection.createStatement();
			     ResultSet mResultSet = mConnectionStatement.executeQuery(mQuery);
			     
			     System.out.println("Query successfully");
			     cerrarDataBase();
			     return mResultSet;  
			  } catch (SQLException e) {
			     e.printStackTrace();
			     System.err.println(e.getClass().getName()+": "+e.getMessage());
			     //System.exit(0);
			     return null; 
			  }
	   }
	   
	   
	   
}
