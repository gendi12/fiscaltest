package impresion;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.BoxLayout;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ListSelectionModel;

public class PanelItems extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTable table;

	private String [] tableTitle= {"Descripcion", "Cantidad","Monto"};
	private String [][] dato = {{"",null,null}};

	/**
	 * Create the panel.
	 */
	public PanelItems() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		DefaultTableModel model = new DefaultTableModel(dato,tableTitle);
		
		table = new JTable( model );
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setSelectionBackground(Color.CYAN);
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER){
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					model.addRow(new Object[]{});
				}else if (arg0.getKeyCode() == KeyEvent.VK_DELETE){
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					model.removeRow(table.getSelectedRow());
				}
				getTableItems();
			}
		});
		
		JScrollPane jScroll = new JScrollPane(table);
		add(jScroll);

	}
	
	public ArrayList<String[]> getTableItems(){
		ArrayList<String[]> datos = new ArrayList<String[]>();
		
		for(int x = 0; x < table.getRowCount(); x++){
			String[] row = {
					(String) table.getModel().getValueAt(x, 0),
					(String) table.getModel().getValueAt(x, 1),
					(String) table.getModel().getValueAt(x, 2),
			};
			datos.add(row);
			
		}
		System.out.println(datos.toString());
		return datos;
	}
	
	public void setTableItems(ArrayList<String[]> data){
		DefaultTableModel model;
		if (data == null) model = new DefaultTableModel(dato,tableTitle);
		else model = new DefaultTableModel(data.toArray(new String[][]{}),tableTitle);
		table.setModel(model);
	}
	
}
