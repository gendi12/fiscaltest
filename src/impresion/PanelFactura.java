package impresion;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PanelFactura extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textFactura;
	JRadioButton rdbtnNewRadioButton;
	JRadioButton rdbtnNewRadioButton_1;
	
	public interface OnCompleteListener {
	    void OnComplete(String text);
	}

	private OnCompleteListener listenerFactura;
	/**
	 * Create the panel.
	 */
	public PanelFactura(OnCompleteListener listener) {
		this.listenerFactura = listener;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{43, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0};
		gridBagLayout.rowWeights = new double[]{Double.MIN_VALUE, 1.0};
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "----", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		setLayout(gridBagLayout);

		
		ButtonGroup grupo = new ButtonGroup();
		
		textFactura = new JTextField();
		textFactura.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER || arg0.getKeyCode() == KeyEvent.VK_TAB){
					action1();
					listenerFactura.OnComplete(textFacturaString());
				}
			}
		});
		
		
		GridBagConstraints gbc_textFactura = new GridBagConstraints();
		gbc_textFactura.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFactura.gridwidth = 2;
		gbc_textFactura.insets = new Insets(0, 0, 5, 5);
		gbc_textFactura.gridx = 0;
		gbc_textFactura.gridy = 0;
		gbc_textFactura.anchor = GridBagConstraints.NORTH;
		add(textFactura, gbc_textFactura);
		textFactura.setColumns(10);
		
		rdbtnNewRadioButton = new JRadioButton("A");
		rdbtnNewRadioButton.addMouseListener(mouseClick());
		rdbtnNewRadioButton.setSelected(true);
		GridBagConstraints gbc_rdbtnNewRadioButton = new GridBagConstraints();
		gbc_rdbtnNewRadioButton.anchor = GridBagConstraints.WEST;
		gbc_rdbtnNewRadioButton.fill = GridBagConstraints.VERTICAL;
		gbc_rdbtnNewRadioButton.insets = new Insets(0, 0, 0, 5);
		gbc_rdbtnNewRadioButton.gridx = 0;
		gbc_rdbtnNewRadioButton.gridy = 1;
		add(rdbtnNewRadioButton, gbc_rdbtnNewRadioButton);
		
		rdbtnNewRadioButton_1 = new JRadioButton("B");
		rdbtnNewRadioButton_1.addMouseListener(mouseClick());
		GridBagConstraints gbc_rdbtnNewRadioButton_1 = new GridBagConstraints();
		gbc_rdbtnNewRadioButton_1.anchor = GridBagConstraints.WEST;
		gbc_rdbtnNewRadioButton_1.gridx = 1;
		gbc_rdbtnNewRadioButton_1.gridy = 1;
		add(rdbtnNewRadioButton_1, gbc_rdbtnNewRadioButton_1);
		
		grupo.add(rdbtnNewRadioButton);
		grupo.add(rdbtnNewRadioButton_1);
	}

	private void action1(){
		String letters = textFactura.getText().replaceAll("[0-9^-]", "").toUpperCase();
		System.out.println("Letra " + letters);
		if (!letters.isEmpty() && letters != null){
			if (letters.charAt(0) == 'A')
				rdbtnNewRadioButton.setSelected(true);
			else if (letters.charAt(0) == 'B')
				rdbtnNewRadioButton_1.setSelected(true);
		}
	}
	
	public void setFracturaString(String arg0){
		textFactura.setText(arg0);
	}
	
	public String textFacturaString(){
		String text = textFactura.getText();
		if (text.length() <= 8) return text.replaceAll("[^\\d.]", "");
		else return text.replaceAll("[^\\d-]", "");
	}
	
	public String getFacturaType(){
		if (rdbtnNewRadioButton.isSelected()) return "A"; 
		if (rdbtnNewRadioButton_1.isSelected()) return "B";
		return null;
	}
	
	public MouseAdapter mouseClick(){
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				listenerFactura.OnComplete(textFacturaString());
			}

		};
	}
	
	
}
